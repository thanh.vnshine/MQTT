package com.app.androidkt.mqtt;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet;
import com.google.gson.Gson;
import com.mikhaellopez.circularprogressbar.CircularProgressBar;

import org.eclipse.paho.android.service.MqttAndroidClient;
import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.MqttCallbackExtended;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    private MqttAndroidClient client;
    private String TAG = "MainActivity";
    private PahoMqttClient pahoMqttClient;

    private MqttAndroidClient mqttAndroidClient;
    private CircularProgressBar pbGas;
    private TextView txtGas;
    private Handler handler;
    private LineData lineData;
    private LineDataSet lineDataSet;
    private ArrayList<ILineDataSet> dataSets;
    private ArrayList<Entry> yVal;
    private ArrayList<Entry> yValueInChart = new ArrayList<>();
    private ArrayList<String> tempList = new ArrayList<>();
    private int gas;
    private LineChart lineChart;
    private MqttHelper mqttHelper;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

//        initData();
//        initControl();
        initView();
        try {
            startMqtt();
        } catch (MqttException e) {
            e.printStackTrace();
        }
    }

    private void startMqtt() throws MqttException {
        mqttHelper = new MqttHelper(getApplicationContext());
        mqttHelper.setCallback(new MqttCallbackExtended() {
            @Override
            public void connectComplete(boolean b, String s) {

            }

            @Override
            public void connectionLost(Throwable throwable) {

            }

            @Override
            public void messageArrived(String topic, MqttMessage mqttMessage) throws Exception {
                System.out.println("SUBSCRIBE "+ mqttMessage.toString());
                Gson gson = new Gson();
                Models models = gson.fromJson(new String(mqttMessage.getPayload()), Models.class);
                gas = models.getGas();
                System.out.println("PARSE "+ gas);
            }

            @Override
            public void deliveryComplete(IMqttDeliveryToken iMqttDeliveryToken) {

            }
        });
    }

//    private void initData() {
//        pahoMqttClient = new PahoMqttClient();
//        client = pahoMqttClient.getMqttClient(getApplicationContext(), Constants.MQTT_BROKER_URL, Constants.CLIENT_ID);
//    }
//
//    private void initControl() {
//
//        mqttAndroidClient = pahoMqttClient.getMqttClient(getApplicationContext(), Constants.MQTT_BROKER_URL, Constants.CLIENT_ID);
//
//
//        mqttAndroidClient.setCallback(new MqttCallbackExtended() {
//            @Override
//            public void connectComplete(boolean b, String s) {
//
//            }
//
//            @Override
//            public void connectionLost(Throwable throwable) {
//
//            }
//
//            @Override
//            public void messageArrived(String s, MqttMessage mqttMessage) throws Exception {
//                Gson gson = new Gson();
//                Models models = gson.fromJson(new String(mqttMessage.getPayload()), Models.class);
//                gas = models.getGas();
//                System.out.println("PARSE "+ gas);
//            }
//
//            @Override
//            public void deliveryComplete(IMqttDeliveryToken iMqttDeliveryToken) {
//
//            }
//        });
//    }
    Runnable runnable = new Runnable() {
        @Override
        public void run() {
            txtGas.setText(""+gas);
            System.out.println("GAS Now " + gas);
            pbGas.setProgress(gas);
            if (tempList.size() > 7) {
                tempList.remove(0);
                tempList.add(gas + "");
                yValueInChart.remove(0);
                yValueInChart.add(new Entry(yValueInChart.size(), gas));
                for (int i = 0; i < yValueInChart.size(); i++) {
                    yValueInChart.get(i).setX(i);
                }
            } else {
                tempList.add(gas + "");

                yValueInChart.add(new Entry(yValueInChart.size(), gas));
            }

            lineDataSet = new LineDataSet(yValueInChart, "Gas");
            lineDataSet.setDrawValues(false);
            lineDataSet.setColor(Color.RED);
            lineData = new LineData();
            lineData.addDataSet(lineDataSet);
            lineChart.setData(lineData);
            lineChart.getXAxis().setDrawLabels(false);
            lineChart.fitScreen();
            lineChart.invalidate();

            handler.postDelayed(this, 1000);

        }
    };
    private void initLine() {
        handler = new Handler();
        handler.postDelayed(runnable, 1000);
    }
    private void initView() {
        pbGas = (CircularProgressBar) findViewById(R.id.pb_gas);
        pbGas.setBackgroundColor(getResources().getColor(R.color.trongGAS));
        pbGas.setBackgroundProgressBarWidth(20);
        pbGas.setColor(getResources().getColor(R.color.ngoaiGAS));
        pbGas.setProgressBarWidth(40);
        txtGas = (TextView) findViewById(R.id.txt_gas);
        lineChart = (LineChart) findViewById(R.id.line_chart);
        dataSets = new ArrayList<>();
        yVal = new ArrayList<>();
        yValueInChart.add(new Entry(0, 0));
        initLine();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        handler.removeCallbacks(runnable);
    }
}
