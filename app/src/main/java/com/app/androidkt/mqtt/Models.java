package com.app.androidkt.mqtt;

import java.io.Serializable;

public class Models implements Serializable {
    private String method;
    private int id;
    private int gas;
    public Models(){

    }
    public Models(String method, int id, int gas) {
        this.method = method;
        this.id = id;
        this.gas = gas;
    }

    public String getMethod() {
        return method;
    }

    public void setMethod(String method) {
        this.method = method;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getGas() {
        return gas;
    }

    public void setGas(int gas) {
        this.gas = gas;
    }
}
